fetch("https://jsonplaceholder.typicode.com/todos",) 
.then(response => response.json())
.then(response => console.log(response));

fetch("https://jsonplaceholder.typicode.com/todos/1",) 
.then(response => response.json())
.then(response => console.log(response));

fetch("https://jsonplaceholder.typicode.com/todos",) 
.then(response => response.json())
.then((response) => {
	let title = response.map((todo =>{
		return todo.title;
	}))
	console.log(title);
});

// .then(json => {console.log(json.map(todo => (todo.title)))})
fetch("https://jsonplaceholder.typicode.com/todos/1",) 
.then(response => response.json())
.then(response => console.log(`The item "${response.title}" on the list has a status of "${response.completed}"`));


fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "POST",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			completed: false,
			title: "Created To Do List Item",
			id: 201,
			userId: 1
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));

fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PUT",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			dateCompleted: "Pending",
			description: "To update the my to do list with a different data structure",
			id: 1,
			status: "Pending",
			title: "Updated To Do List Item",
			userId: 1
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));


fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "PATCH",
		headers: {
			"Content-Type": "application/json"
		},
		body: JSON.stringify({
			completed: false,
			dateCompleted: "07/09/21",
			id: 1,
			status: "Complete",
			title: "delectus aut autem",
			userId: 1
		})
	}
)
.then(response => response.json())
.then(response => console.log(response));


fetch("https://jsonplaceholder.typicode.com/posts/1", 
	{
		method: "DELETE"
	}
)
.then(response => response.json())
.then(response => console.log(response));

